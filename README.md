
## Install

```bash
yarn install

```
## Utilisation

```bash
# Lance les taches clean,style et genere la styleguide
yarn styleguide
# Lance la tache par default de gulp
yarn build
# gulp work
yarn work

# Gulp tasks
#
gulp | gulp build
gulp styles
gulp styleguide
gulp templates


```




## Templating

https://mozilla.github.io/nunjucks/fr/templating.html

| filter        | Description    |
| :-------------| :------------- |
| `markdown`    | Markdown-it    |
| `slug`        | replace space with `-`|
| 'dump(<indent>)' | json stringify |



## Styleguide

https://github.com/kss-node/kss/blob/spec/SPEC.md
