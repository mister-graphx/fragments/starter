const gulp = require('gulp')
// https://lodash.com/docs/4.17.15
const _ = require('lodash')
const log  = require('fancy-log');
const sass = require('gulp-sass')
const eyeglass = require('eyeglass')
const del = require('del')
const postcss = require('gulp-postcss')
const browserSync = require('browser-sync').create()

global._SCSS_PATH = 'src/_scss/'
global._BUID_PATH = 'public/'

let config = {
  mode: 'development',
  browserSync: {
        server: {
            baseDir: _BUID_PATH
        }
  },
  sass:{
    outputStyle: 'nested',
    includePaths: [

    ],
    eyeglass:{
      useGlobalModuleCache: false,
      enableImportOnce: false
    }
  },
  //  markdown-it
  markdown:{
    html: true,
    linkify: true,
    typographer: true
  },
  //  nunjucks
  nunjucks: {
    path: ['src/templates/','src/images/','src/datas/'],
    ext: '.html',
    data: {},
    inheritExtension: false,
    envOptions: {
      autoescape: false,
      watch: false,
      noCache: true
    },
    loaders: null
  },
  // https://github.com/kangax/html-minifier#options-quick-reference
  htmlmin:{
    collapseWhitespace: true
  },
  // https://github.com/beautify-web/js-beautify#options
  beautify:{
    html:{
      'max-preserve-newlines': 1,
      indent_size: 2
    }
  },
  // https://github.com/postcss/postcss-url
  'postcssUrl': [
    {
      url: 'inline',
      basePath: 'images/'
    },
    { filter: 'images/*.svg', url: 'inline' },
  ]
}

// node env mode production/development
process.env.NODE_ENV = process.env.NODE_ENV || config.mode || 'production';
log.info('Starting task in mode : ' + process.env.NODE_ENV );

// Styles
//
function styles(){
  return gulp.src(_SCSS_PATH + '*.scss')
      .pipe(sass(eyeglass(config.sass)).on('error', sass.logError))
      .pipe(postcss([
            require('autoprefixer'),
            require('postcss-custom-properties'),
            require('postcss-url')(config.postcssUrl)
      ]))
      .pipe(gulp.dest('./public/css/'))
      .pipe(browserSync.stream());
}



const kss = require('kss');
const kssConfig = require('./kss-config.json');
function styleguide(cb){
  // console.log(kssConfig);
  return kss(kssConfig);
  cb();
}
// ----------
// TEMPLATES
// en mode dev formatter le html js, css de manière lisible après des include/import : https://www.npmjs.com/package/gulp-beautify

// https://www.npmjs.com/package/gulp-data
const data = require('gulp-data');
const _if = require('gulp-if');
const nunjucksRender = require('gulp-nunjucks-render');
// Html minification
// https://www.npmjs.com/package/gulp-htmlmin
const htmlmin = require('gulp-htmlmin');
const beautify = require('gulp-beautify');

// @param file - path depuis ./page nom du fichier sans ext
// @return Object
// https://nodejs.org/docs/latest/api/fs.html
const fs = require('fs');
const path = require('path');

// getDatas : hotreload data
// extensions :
//  - json - return a json object
//  - js - return object use require and clear module.caching for this file on each load
//
// @param filePath
// @return object
function getDatas(filePath){
    let datas = {};
    log('getDatas : dataFile :' + filePath);
    if(fs.existsSync(filePath)){
      if(path.extname(filePath) == '.js'){
        try{
          delete require.cache[require.resolve(filePath)]
          datas = require(filePath);
        }
        catch (e) {
          log.error( filePath + "Loading error:", e);
        }
      }
      if(path.extname(filePath) == '.json'){
        try{
          datas = JSON.parse(fs.readFileSync(filePath,'utf8'));
        }
        catch (e) {
          log.error("Parsing error:", e);
        }
      }
      if(path.extname(filePath) == '.yaml'){
        const YAML = require('yaml');
        try{
          datas = YAML.parse(fs.readFileSync(filePath,'utf8'));
        }
        catch (e) {
          log.error("Parsing error:", e);
        }
      }
      return datas;
      // csv import
    }else{
      log('getDatas : No file found for :' + filePath);
    }
}
//  Markdown render use Markdown-it
//  usage: `|markdown` ord as filter block {% filter markdown %}{% endfilter %}
const markdown = require('markdown-it')(config.markdown);
// https://www.npmjs.com/package/slug
const slug = require("slug");

function templates(cb){

  let options  = config.nunjucks;

  options.data = {
    app: getDatas('./src/datas/app.json'),
    // products: getDatas('./src/datas/products.js')
  };
  options.manageEnv = function(environment) {

      environment.addFilter('slug', function(str) {
        return slug(str);
      });

      environment.addFilter('markdown', function(str,options) {
        return markdown.render(str);
      });
      // load Datas From templates
      // path must be relative to gulp file
      // usage: {% set products = getDatas('./src/datas/products.js') %}
      environment.addGlobal('getDatas', getDatas);

  };
  // log(options.data);
  return gulp.src(['src/templates/*.njk','!src/templates/_*.njk'])
          .pipe(nunjucksRender(options))
          .pipe(_if(function(){
                if(process.env.NODE_ENV == 'production')
                    return true;
              },
              htmlmin(config.htmlmin),
              beautify.html(config.beautify.html))
          )
          .pipe(gulp.dest('./public/'))
          .pipe(browserSync.stream());
  cb();
}

function clean(cb){
  return del([
      'public/*',
  ]);
  cb();
}

// BrowserSync
function serve(done) {
  browserSync.init(config.browserSync);
  done();
}
// BrowserSync Reload
function browserSyncReload(done) {
  browserSync.reload();
  done();
}

function watchFiles() {
  gulp.watch(_SCSS_PATH + '*.scss', (done) => {
      gulp.series(styles,browserSyncReload)(done);
  });
  gulp.watch('src/templates/*.njk', (done) => {
      gulp.series(templates,browserSyncReload)(done);
  });
}

const build = gulp.series(clean,styles,styleguide, templates);
const work = gulp.series(clean,styles,templates,gulp.parallel(serve,watchFiles));

// Export modules
exports.clean = clean;
exports.templates = templates;
exports.reload = browserSyncReload;
exports.styles = styles;
exports.styleguide = styleguide;
exports.default = build;
exports.work = work;
// exports.watch = watch;
