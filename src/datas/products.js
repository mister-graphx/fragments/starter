function products(){

  let products = [
    {
        "name":"Product 1",
        "description":"Product description",
        "lang":"fr",
        "price_ht": '15',
        "price_ttc": '17',
        'shipping': '6.95'
    },
    {
        "name":"Product 2",
        "description":"Product description",
        "lang":"fr",
        "price_ht": '15',
        "price_ttc": '17',
        'shipping': '6.95'
    },
    {
        "name":"Product 3",
        "description":"Product description",
        "lang":"fr",
        "price_ht": '15',
        "price_ttc": '17',
        'shipping': '6.95'
    }
    ,
    {
        "name":"Product 4",
        "description":"Product description",
        "lang":"fr",
        "price_ht": '15',
        "price_ttc": '17',
        'shipping': '6.95'
    }
  ];

  return products;
}
// expose the product list as export with no specific assignation
module.exports = products();
