'use strict';
// const linkify = require('linkify-it')();
const markdown = require('markdown-it')({
  html: true,
  linkify: true,
  typographer: false
});

module.exports = function(nunjucks) {
  nunjucks.addFilter('markdown', (str) => {
    // console.log(typeof str.val);
    if(typeof str.val === 'string' ){
      // console.log(markdown.renderInline(str.val));
      try {
        return markdown.renderInline(str.val);
      } catch (e) {
        console.log(e);
      }
    }
  });
};
