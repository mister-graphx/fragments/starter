'use strict';
//
module.exports = function(nunjucks) {

  let app = {
    "brand": "My App Name",
    "toolbar":{
      "fullscreen": "Toggle fullscreen",
      "newWindow": "Open in new window",
      "guides": "Toggle example guides",
      "markup": "Toggle HTML markup"
    }
  };
  return nunjucks.addGlobal('app',app) ;
};
